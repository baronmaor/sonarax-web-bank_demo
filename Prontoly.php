<?php
	require_once __DIR__.'/lib/Prontoly2FA.php';

	//start new session or load existing session
	session_start();

	define('Prontoly_name', 'Prontoly');

	//Authentication Server
	//define('Prontoly_appID', '587b9ceef6432d7232d8d568');
	//Azure
	define('Prontoly_appID', '5885b8dcc15d63bd5d4aabf5');
	
	// Registration
	if (isset($_POST['Register'])) {

		// get the userID
		if (isset($_POST['userID'])) {
			$user = $_POST['userID'];
		} else {
			$user = "unknown";
		}
		
		$result = Prontoly2FA::Register(Prontoly_appID, $user);
	
		if (isset($result['result']))
		{	
			$_SESSION['second_stage'] = 'login';
			$_SESSION['pronto_user_id'] = $result['result']['user']['id'];

			$result = array('result'=>true, 'action'=>'iframe','url'=>$result['result']['url']);
		} else if (isset($result['error'])) {

			$result = array('Error'=>true, 'Error_Message'=>$result['error']);
		} else {

			$result = array('Error'=>true, 'Error_Message'=>'Failed To Connect To Prontoly API.');
		}
	}
	
	// Login
	if (isset($_POST['Login'])) {

		// get the userID
		if (isset($_POST['userID'])) {
			$user = $_POST['userID'];
		} else {
			$user = "unknown";
		}
		
		$result = Prontoly2FA::Login(Prontoly_appID, $user);
	
		if (isset($result['result']))
		{	
			$_SESSION['second_stage'] = 'reg';
			$_SESSION['pronto_user_id'] = $result['result']['user']['id'];

			$result = array('result'=>true, 'action'=>'iframe','url'=>$result['result']['url']);
		} else if (isset($result['error'])) {

			$result = array('Error'=>true, 'Error_Message'=>$result['error']);
		} else {

			$result = array('Error'=>true, 'Error_Message'=>'Failed To Connect To Prontoly API.');
		}
	}

	// PreRegister
	if (isset($_POST['PreRegister'])) {

		// get the username
		if (isset($_POST['username'])) {
			$username = $_POST['username'];
			$reset = $_POST['reset'];
		} else {
			$result = array('Error'=>true, 'Error_Message'=>'Error retriving username');
			return;
		}
		
		$result = Prontoly2FA::PreRegister(Prontoly_appID, $username, $reset);
	
		if (isset($result['result']))
		{	
			$result = array('result'=>true ,'activationCode'=>$result['result']['activationCode']);
		} else if (isset($result['error'])) {

			$result = array('Error'=>true, 'Error_Message'=>$result['error']);
		} else {

			$result = array('Error'=>true, 'Error_Message'=>'Failed To Connect To Prontoly API.');
		}
	}




// 	$result = array('error'=>true, 'error_message'=>'unknown request.');
// 	if (isset($_POST['ajax']))
// 	{
// 		//SHOULD REPLACE THIS CONSTS WITH YOUR APP_ID & APP_SECRET
// //		define('PRONTO_APP_ID', '576824480e92a7f05001f520');
// //		define('PRONTO_APP_SECRET', 'cutuGXyCvlCy35qu4Vv8nMIOid6e0l62jwzLzkCM2zMjfeXNrM');

// //		define('PRONTO_APP_ID', '55378eca60d198461365d99a');
// // 		define('PRONTO_APP_SECRET', 'IWtSwYtrrKA7TheErO3qbpBzWQOwVEyRGtlfwZRf8vdfnQ0IXU');

// //        //lloydsDemo 5/10
// //        //name: lloyds_demo
//         define('PRONTO_APP_ID', '57f64db3b9580f7c0cb44017');
//         define('PRONTO_APP_SECRET', 'Vc2qPvTT5bTYRWzjbIw8bekg53fZJnPG7BBv02auedsm6BsXzW');

// 		if (is_second_stage_request())
// 		{
// 			//after first stage, do validation
// 			if (is_second_stage_type('login'))
// 			{
// 				//login validation
// 				$pronto_user_id = $_SESSION['pronto_user_id'];
// 				$transaction_id = $_SESSION['transaction_id'];
// 				//request Pronto API if login complete
// 				$result = Pronto2FA::validateTransaction(PRONTO_APP_ID, PRONTO_APP_SECRET, $pronto_user_id, $transaction_id);
// 				if (isset($result['result']))
// 				{
// 					$_SESSION['logged_in'] = true;
// 					unset ($_SESSION['second_stage']);
// 					unset ($_SESSION['pronto_user_id']);
// 					unset ($_SESSION['transaction_id']);
// 					session_regenerate_id(true);
// 					//
// 					$result = array('result'=>true, 'action'=>'logged_in');
// 				}
// 				else
// 				{
// 					$result = array('error'=>true, 'error_message'=>'login validation failed!');
// 				}
// 			}
// 			else if (is_second_stage_type('reg'))
// 			{
// 				//registration validation
// 				$pronto_user_id = $_SESSION['pronto_user_id'];
// 				//request Pronto API if user is registered
// 				$result = Pronto2FA::validateRegistration(PRONTO_APP_ID, PRONTO_APP_SECRET, $pronto_user_id);
// 				if (isset($result['result']))
// 				{
// 					//user registered

// 					//save in "db"
// 					if (enable_mfa_for_admin($pronto_user_id))
// 					{
// 						$_SESSION['logged_in'] = true;
// 						unset ($_SESSION['second_stage']);
// 						unset ($_SESSION['pronto_user_id']);
// 						session_regenerate_id(true);

// 						$result = array('result'=>true, 'action'=>'logged_in');
// 					}
// 					else
// 					{
// 						$result = array('error'=>true, 'error_message'=>'registration failed to save "db" file! try to correct permissions..');
// 					}
// 				}
// 				else
// 				{
// 					$result = array('error'=>true, 'error_message'=>'registration validation failed!');
// 				}
// 			}
// 		}
// 		else if (is_login_request_valid())
// 		{
// 			//"login" is valid
// 			//check if admin has mfa enabled
// 			$pronto_user_id = is_mfa_enabled_for_admin();
// 			if ($pronto_user_id)
// 			{
// 				//admin already registered for mfa, ask for login transaction from Pronto API
// 				$result = Pronto2FA::startTransaction(PRONTO_APP_ID, PRONTO_APP_SECRET, $pronto_user_id);

// 				if (isset($result['result']))
// 				{
// 					//save data in session to use in validation phase later
// 					$_SESSION['second_stage'] = 'login';
// 					$_SESSION['pronto_user_id'] = $pronto_user_id;
// 					$_SESSION['transaction_id'] = $result['result']['id'];

// 					$result = array('result'=>true, 'action'=>'iframe', 'url'=>$result['result']['url']);
// 				}
// 				else
// 				{
// 					//$result = array('error'=>true, 'error_message'=>'failed to get transaction from Pronto API. check API error.');
// 				}
// 			}
// 			else
// 			{
// 				//not registered yet, ask for registration transaction from Pronto API
// 				$result = Pronto2FA::startRegistration(PRONTO_APP_ID, PRONTO_APP_SECRET, 'admin');
// 				if (isset($result['result']))
// 				{
// 					//save data in session to use in validation phase later
// 					$_SESSION['second_stage'] = 'reg';
// 					$_SESSION['pronto_user_id'] = $result['result']['user']['id'];

// 					$result = array('result'=>true, 'action'=>'iframe','url'=>$result['result']['url']);
// 				}
// 				else
// 				{
// 					//$result = array('error'=>true, 'error_message'=>'failed to get registration transaction from Pronto API. check API error.');
// 				}
// 			}
// 		}
// 	}

// 	function is_second_stage_request()
// 	{
// 		return (isset($_POST['validate']) && isset($_SESSION['second_stage']));
// 	}

// 	function is_login_request_valid()
// 	{
// 		// return (isset($_POST['username']) && isset($_POST['password']) &&
// 		//         $_POST['username'] == 'admin' && $_POST['password'] == 'admin');
// 		return true;
// 	}

	function is_second_stage_type($type)
	{
		return (($type == 'login' && $_SESSION['second_stage'] == 'login') ||
		        ($type == 'reg' && $_SESSION['second_stage'] == 'reg')) ? true : false;
	}

// 	/**
// 	* check if admin has mfa (registered already)
// 	*/
// 	function is_mfa_enabled_for_admin()
// 	{
// 		$path = sys_get_temp_dir().'/pronto_db_lloyds2';
// 		if (file_exists($path))
// 		{
// 			return file_get_contents($path);
// 		}
// 		return false;
// 	}

// 	/**
// 	* enable mfa for admin user (save it in "db" file)
// 	*/
// 	function enable_mfa_for_admin($pronto_user_id)
// 	{
// 		$path = sys_get_temp_dir().'/pronto_db_lloyds2';
// 		return file_put_contents($path, $pronto_user_id);
// 	}

// 	function delete_mfa_for_admin()
// 	{
// 		$path = sys_get_temp_dir().'/pronto_db_lloyds2';
// 		unlink($path);
// 	}


	//echo result to client
	header('Content-Type: application/json', true);
	echo json_encode($result);
	die;

