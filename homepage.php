<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sonarax Bank - Ultrasonic Authentication</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/prontoly.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="homepageBody">

<img id="ProntolyMap" src="images/IB3.png" border="0" width="1280" height="2441" orgWidth="1280" orgHeight="2441" usemap="#map" alt="" />
<map name="map" id="ProntolyMap">
    <area  alt="" title="" href="index.php" shape="rect" coords="1022,79,1111,140" style="outline:none;" target="_self"/>
    <area shape="rect" coords="1278,2439,1280,2441" style="outline:none;" href="index.php">
</map>




    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!--Prontoly -->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="lib/script.js"></script>
    <!--End Prontoly -->
</body>

</html>
