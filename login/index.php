<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Prontoly Bank - Ultrasonic Authentication</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/prontoly.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                &nbsp;
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="loggedIn">
                    <ul class="nav navbar-nav">
                        <li>
                            <a class="linkBullet newwin" href="#" title="Cookie policy: Opens in a new window">Cookie policy</a>
                        </li>
                        <li>
                            <a href="#" title="Mobile" class="linkBullet">Mobile</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container content">

        <!-- Projects Row -->
        <div class="row">
            <div class="col-md-7-10 primary portfolio-item">
                <div class="navbar-prontoly">
                    <a href="#">
                        <img class="img-responsive ProntolyLogo" src="images/Prontoly-Logo.png" alt="ProntolyLogo">
                    </a>
                </div>
                <div id="bigFirstStep" class="col-md-12 panel">
                    <h1>
                        Welcome to Prontoly ultrasonic authentication
                    </h1>
                    <div class="inner">
                        <p>If you don't already use Internet Banking, it's simple to&nbsp;<a href="#" title="register online">register online</a>.</p>
                        <div>
                            <p style="padding-bottom: 15px;">
                                <strong>Please log in using your Prontoly Bank mobile app to authenticate ultrasonically.</strong>
                            </p>
                        </div>
                        <div>
                            <div>
                                <div id="firstStep" class="subPanel prontolyPanel">
                                    <div style="min-height: 210px;">
                                        <p>
                                            <img src="images/phone.png" style="float:right; margin-top: 20px;">
                                        </p>
                                        <p>
                                            <strong>Log on with your Prontoly Bank mobile app</strong>
                                        <p>
                                        <p>
                                            Use ultrasonic authentication to securely log on with the app now.
                                        </p>

                                        <div>
                                            How it works
                                            <div class="tooltipProntoly">
                                                <span class="cxtHelp">
                                                    <a class="cxtTrigger" title="" href="#cxtHelp1">[?]</a>
                                                </span>
                                                <span class="tooltipProntolytext">
                                                </span>
                                            </div>
                                        </div>
                                        <p>
                                        <div id="usernameUltrasonic" class="formField" style="max-width: 180px;">
                                            <div class="formFieldInner">
                                                <label for="UserIdUltrasonic">User ID:</label>
                                                <input type="text" autocomplete="off" id="UserIdUltrasonic" name="UserIdUltrasonic" class="field setFocus" maxlength="30" value="user">
                                                <span id="errUsernameUltrasonic" class="error hidden" aria-live="polite" aria-atomic="true" role="alert">Not valid.</span>
                                            </div>
                                        </div>
                                        <div>
                                        </div>
                                        </p>
                                    </div>
                                    <div class="loginActions clearfix" style="margin-top: -19px;">
                                        <form id="login_form">
                                            <input type="submit" class="loginAction" value="Login" onclick="Login(); return false;">
                                        </form>
                                        <form id="login_form">
                                            <input type="submit" class="loginAction" value="Register" onclick="Register(); return false;">
                                        </form>
                                    </div>
                                    </p>
                                    <div id="cleared"></div>
                                    <div class="site_second_phase">
                                        <div id="site_mask" class="hidden"></div>
                                        <div id="site_background" class="hidden"></div>
                                        <div id="site_loading_icon" class="hidden"><img src="images/loading_icon.gif"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 panel" style="margin-top: 20px;"></div>
                </div>

            </div>
        <!-- Sidebar -->
        <div class="col-md-3-10 secondary portfolio-item">
            <div class="secureMsg">
                <p class="msg">
                    <img src="images/secure_msg.png" alt="test">
                </p>
                <p><a class="linkBullet newwin" href="admin.php" title="test">register / reset users for the service</a></p>
            </div>
            <div class="panel">
                <div class="subPanel">
                    <p>
                        <a class="newwin" href="#" title="test">
                            <img alt="test" src="images/car-finance-plus-tile.png">
                        </a>
                    </p>
                </div>
                <div class="subPanel text-center">
                    <p>
                        <a class="newwin" href="#" title="test">
                            <img alt="test" src="images/aeries-log-on-tile-lloyds.jpg">
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <!-- /.Sidebar -->
    </div>
        <!-- /.row -->

        <!-- Footer -->
        <div id="footer">
            <div class="outer">
                <div id="footerInner">
                    <ul>
                        <li><a class="newwin" href="#" title="Legal: Opens in a new window">Legal</a></li>
                        <li><a class="newwin" href="#" title="Privacy: Opens in a new window">Privacy</a></li>
                        <li><a class="newwin" href="#" title="Security: Opens in a new window">Security</a></li>
                        <li><a class="newwin" href="http://prontoly.com" title="www.prontoly.com: Opens in a new window">www.prontoly.com</a></li>
                        <li><a class="newwin" href="#" title="Rates and Charges: Opens in a new window">Rates and Charges</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!--Prontoly -->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <!--<script type="text/javascript" src="http://40.117.230.177:3000/iframe/js/Prontoly.min.js"></script>-->
    <script type="text/javascript" src="http://server.prontoly.com:3000/iframe/js/Prontoly.min.js"></script>
    <script type="text/javascript" src="lib/script.js"></script>
    <!--End Prontoly -->
</body>

</html>
