<?php


//define('BASE_SERVER', 'http://40.117.230.177:3000');
define('BASE_SERVER', 'http://server.prontoly.com:3000');
define('URL_LOGIN_START', BASE_SERVER.'/transaction/start');
define('URL_LOGIN_VALIDATION', BASE_SERVER.'/transaction/validate');
define('URL_REGISTRATION_START', BASE_SERVER.'/user/register');
define('URL_REGISTRATION_VALIDATION', BASE_SERVER.'/registration/validate');

define('Url_Application_Register', BASE_SERVER.'/application/register');
define('Url_User_Register', BASE_SERVER.'/user/register');
define('Url_User_PreRegister', BASE_SERVER.'/user/preregister');
define('Url_User_Login', BASE_SERVER.'/transaction/start');


class Prontoly2FA
{
	public static function PreRegister($appID, $username, $reset)
    {
	        $data = array(
	        'appID' => $appID,
			'username' => $username,
			'reset' => $reset,
    	    );
        return self::_post($data, Url_User_PreRegister);
    }

	public static function Register($appID,$username)
    {
	        $data = array(
	        'appID' => $appID,
			'username' => $username
    	    );
        return self::_post($data, Url_User_Register);
    }

	public static function Login($appID,$username)
    {
	        $data = array(
	        'appID' => $appID,
			'username' => $username
    	    );
        return self::_post($data, Url_User_Login);
    }

	/**
	 * @param $name
	 * @param $expiration
	 *
	 * @return mixed
	 */
	public static function ApplicationRegister($name, $expiration)
    {
	        $data = array(
	        'name' => $name,
			'expiration' => $expiration
    	    );
        return self::_post($data, Url_Application_Register);
    }

	/********************************* OLD *********************************/
	/**
	 * @param $app_id
	 * @param $app_secret
	 * @param $app_uid
	 *
	 * @return mixed
	 */
    public static function startTransaction($app_id,$app_secret,$app_uid)
    {
	        $data = array(
	        'app_id' => $app_id,
	        'app_secret' => $app_secret,
	        'app_uid' => $app_uid
    	        );
        return self::_post($data,URL_REGISTRATION_START);
    }

	/**
	 * @param $app_id
	 * @param $app_secret
	 * @param $username
	 *
	 * @return mixed
	 */
    public static function startRegistration($app_id,$username)
    {
        $data = array(
	        'appID' => $app_id,
	        'username' => $username
        );
	    return self::_post($data,URL_REGISTRATION_START);
    }


	/**
	 * @param $app_id
	 * @param $app_secret
	 * @param $app_uid
	 * @param $transaction_id
	 *
	 * @return bool
	 */
    public static function validateTransaction($app_id,$app_secret,$app_uid,$transaction_id)
    {
        $data = array(
	        'app_id' => $app_id,
	        'app_secret' => $app_secret,
	        'app_uid' => $app_uid,
	        'trans_id' => $transaction_id
        );
        return self::_post($data,URL_LOGIN_VALIDATION);
    }

	/**
	 * @param $app_id
	 * @param $app_secret
	 * @param $app_uid
	 *
	 * @return bool
	 */
    public static function validateRegistration($app_id,$app_secret,$app_uid)
    {
        $data = array(
	        'app_id' => $app_id,
	        'app_secret' => $app_secret,
	        'app_uid' => $app_uid
        );
	    return self::_post($data,URL_REGISTRATION_VALIDATION);
    }


	/**
	 * Send POST request using CURL
	 *
	 * @param $post_data
	 * @param $url
	 *
	 * @return mixed
	 */
    private static function _post($post_data, $url)
    {
        //url-ify the data for the POST
	    $fields_string = '';
        foreach ($post_data as $key=>$value)
        {
	        $fields_string .= $key.'='.$value.'&';
        }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,7);
	    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_POST, count($post_data));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        $result = curl_exec($ch);




        //close connection
        curl_close($ch);

        return json_decode($result, true);
    }
} 
